#!/bin/bash
set -e
# Configuracion mysql

#Inicializamos el servicio de mysql
/etc/init.d/mysql start

#Esperamos unos segundos a que mysql termine de inicializarse
sleep 5
#Creacion del usuario admin de PMA
mysql -u root -e "CREATE USER '$PMA_USER'@'localhost' IDENTIFIED BY '$PMA_PASS';"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$PMA_USER'@'localhost';"

#Tenemos 2 opciones, la primera es mediante el volumen /var/www/html pasarle los archivos de phpmyadmin, o instalarlo en el entrypoint
#Si el directorio phpmyadmin dentro del volumen esta vacio, instalara la version que le pasamos mediante la variable de entorno
if [ -z "$(ls -A /var/www/html/phpmyadmin)" ]; then
    #Nos posicionamos en el directorio /tmp asi no dejamos ningun archivo extra en el /var/www/html
    cd /tmp
    #Descagamos el archivo de phpmyadmin comprimido
    wget https://files.phpmyadmin.net/phpMyAdmin/$PHP_VER/phpMyAdmin-$PHP_VER-all-languages.tar.gz
    #Lo descomprimimos
    tar -zxvf phpMyAdmin-$PHP_VER-all-languages.tar.gz
    #Lo movemos hacia el /var/www/html
    mv phpMyAdmin-$PHP_VER-all-languages /var/www/html/phpmyadmin
    #Y le damos permisos al user www-data sobre el directorio y todos sus archivos
    chown www-data.www-data /var/www/html/phpmyadmin/* -R
fi
#Configuracion de apache para phpmyadmin

#Habilitamos el modulo rewrite para convertir urls dinamicas en urls mas amigables
a2enmod rewrite
#Agregamos al apache.conf las siguientes lineas de configuracion
echo "<Directory /var/www/html>" >> /etc/apache2/apache2.conf
#El allow override nos permitira modificar de forma dinamica partes de la configuracion, necesario para el phpmyadmin
echo "AllowOverride ALL" >> /etc/apache2/apache2.conf
echo "</Directory>" >> /etc/apache2/apache2.conf
#Le asignamos un servername de Localhost
echo "ServerName 127.0.0.1" >> /etc/apache2/apache2.conf

apachectl -D FOREGROUND
exec "$@"